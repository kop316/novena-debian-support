Novena Debian Support
=====================

***NOTE: If you are using my u-boot from the below link, these files are no longer needed. There is a helper script in my u-boot.

https://github.com/chris4795/u-boot-novena

I am keeping this up in case you are using xobs' u-boot.

In the default novena set up, the initramfs is installed in /usr/share/linux-novena. These scripts moves that image to /boot (so that anytime apt updates the image, the correct one is there) and additionally creates a u-boot compatible uinitrd so that it can use the initramfs image. This is done mainly to support full disk encryption on the novena, but this is how Debian normally operates, so I imagine it fixes other issues as well. These scripts also update the u-boot image anytime that "update-initramfs" is called.

To install them, take all of the files (with the exception of "debian", ".gitignore", and "README") and place them in the folder location that I have them. For example, zzz-copy-to-sd is located in /etc/kernel/postinit.d/zzz-copy-to-sd in the github, and that is where it needs to go in your computer. If there is not a folder location (i.e. /etc/initramfs/post-update.d/), you must create it.

These scripts are directly designed to be compatible with the kernel from:

https://github.com/chris4795/linux-novena-4.4

or if you built it yourself, the config files from

https://github.com/chris4795/Linux_Config_Novena

If you do not install these scripts in the proper place and have full disk encryption, your novena will not boot!

After you place these scripts, please reinstall your image so that the scripts can do what they need to do for proper initramfs support. If you are still on the kosagi repo, run 

```console
~ # apt-get install --reinstall linux-image-novena
```
If you are on a custom built one, use 

```console
~ # dpkg -i $name_of_kernel_Image.deb
```
There are no longer any known issues with these scripts.
